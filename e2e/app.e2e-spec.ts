import { BarcodeManagementFrontendPage } from './app.po';

describe('barcode-management-frontend App', function() {
  let page: BarcodeManagementFrontendPage;

  beforeEach(() => {
    page = new BarcodeManagementFrontendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
