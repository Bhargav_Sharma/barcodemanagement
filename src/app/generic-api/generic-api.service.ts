import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Http, Response, Headers } from '@angular/http';
import { environment } from '../../environments/environment';
import 'rxjs/Rx';


@Injectable()
export class GenericApiService {

    constructor(private _http: Http) {
    }

    public GetAll = (actionUrl: string): Observable<any> => {
        return this._http.get(environment.barcodesAPIServer_Dev+actionUrl)
            .map((response: Response) => <any>response.json())
            .do(x => x);

    }

    public PostAll = (actionUrl: string, body: any) :  Observable<any> => {

        var response = {};

        return this._http.post(environment.barcodesAPIServer_Dev+actionUrl, body)
            .map((response: Response) => <any>response.json())
            .do(y => y);
    }

    saveJwt(jwt) {
        if (jwt) {
            localStorage.setItem('id_token', jwt)
        }
    }

}
