/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { GenericApiService } from './generic-api.service';

describe('GenericApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GenericApiService]
    });
  });

  it('should ...', inject([GenericApiService], (service: GenericApiService) => {
    expect(service).toBeTruthy();
  }));
});
