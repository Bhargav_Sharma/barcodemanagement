import { NgModule, Component, OnInit, ViewChild, Directive, Input, ContentChildren, QueryList } from '@angular/core';
import { BarcodeService } from './services/barcode.service';
import { BatchDetailsResponse } from './models/batchdetailsresponse';
import { BatchBarcodesResponse } from './models/batchbarcodesresposnse';
import { NgForm } from '@angular/forms'
import 'rxjs/Rx';
import {Subscription} from 'rxjs';
import {BrowserModule } from '@angular/platform-browser';
import {Constants} from './constants/constants';
import {Utility} from '../shared/utility.component';


@Component({
  moduleId: module.id,
  selector: 'barcodeGen',
  templateUrl: 'barcode_generation.component.html',
  providers: [BarcodeService,Constants,Utility]
})

export class BarcodeGenerationComponent {
  @ViewChild('f') public myForm: NgForm;
  
  public currentStep: string;
  public frm: any;
  numberPattern: string;
  showErrors: boolean;
  transactionMessage: boolean;
  showTable: boolean;
  public values: { Message: string; StatusCode: string; section: any; ReturnId: string };
  public response: any[];

  constructor(private barcodeService: BarcodeService, private utility:Utility, private constants:Constants) {
    console.log("Batch service constructor...");
    this.frm = {};
    this.initForm();
    this.showTable = false;
  }

  initForm() {
    this.frm = {};

    this.numberPattern = "^([1-9][0-9]*||[0]*[1-9]+[0-9]*)$";
    this.showErrors = false;
    this.frm.Country = "";
    this.frm.Kittype = "";
    this.transactionMessage = false;
  }

  AddBatchclicked() {

    var country = this.frm.Country;
    var kitType = this.frm.Kittype;
    var quantity = this.frm.Quantity;
    this.transactionMessage = false;
    
     this.barcodeService.createBatch(country, kitType, quantity)
      .subscribe(
      data => {this.values = data;},
      error => {console.log(error);
          this.transactionMessage = true;
          this.frm.message = "Unable to create batch for the request, please try again!";
        },
      () => {
      //Getting data and binding to the Grid/Table
      console.log(this.values.Message);

      if(this.values.StatusCode == "201")
      {
        var batchId = this.values.ReturnId;
        this.GetBatch(batchId);

        if (this.values === undefined || this.values.StatusCode != "201")
        {
          this.transactionMessage = true;
          this.frm.message = "Unable to create batch for the request, please try again!";
        }
        else {
          this.initForm();
          this.transactionMessage = true;
          this.showTable = true;
          this.frm.message = this.values.Message;
        }
      }
      else
      {
         this.transactionMessage = true;
         this.frm.message = "Unable to create batch for the request, please try again!";
      }
    });
  }

  public batches: BatchDetailsResponse[] = [];
  public batchDetailsResponse: BatchDetailsResponse;
  public batchBarcodesResponse: BatchBarcodesResponse;
  GetBatch(batchId) {
    console.log('Get batch is called');
    this.response = this.barcodeService.getBatchDetails(batchId)
      .subscribe(data => {
      this.batchDetailsResponse = data;
      },
      error => console.log(error),
      () => {
        console.log(this.batchDetailsResponse);
        this.batches.push(this.batchDetailsResponse);

        //Removing Undefined elements
        this.batches = this.batches.filter(function (element) {
          return element !== undefined;
        });

        this.GetBatchBarcodes();
      });
  }

  GetBatchBarcodes()
  {
    var batchNumberArray: any[] = [];
    for (let item in this.batches) {
      if (this.batches[item] !== undefined)
        batchNumberArray.push(this.batches[item].BatchNumber);
    }

    if (batchNumberArray.length != 0)
      this.response = this.barcodeService.getBatchBarcodeDetails(batchNumberArray)
        .subscribe(data => this.batchBarcodesResponse = data,
        error => console.log(error),
        () => console.log(this.batchDetailsResponse));
  }

  ExportToCSVClick() {
    var header = 'BatchNumber,Country,KitType,Barcodes,CreatedBy,CreatedOn,,';
    var csvData = this.utility.ConvertToCSV(this.batchBarcodesResponse, this.constants.header);
    this.utility.ExportToCSV(csvData, this.constants.fileName);
    this.initForm();
    this.showTable = false;
    this.batchBarcodesResponse = new BatchBarcodesResponse();
    this.batches = []; 
  }

  CancelBatch(batchId) {
    confirm("Do you really want to cancel the batch with batch number:" + batchId + "?");
    //Remove the element from this.batchBarcodesResponse and batchNumberArray
    //Call the Service method to remove the data from DB
  }


}

