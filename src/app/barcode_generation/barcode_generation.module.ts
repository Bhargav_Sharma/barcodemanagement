import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BarcodeGenerationComponent } from './barcode_generation.component';
@NgModule({  
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
  ],
  declarations: [
    BarcodeGenerationComponent
  ],
  exports:[BarcodeGenerationComponent], 
  providers: [  ],
  bootstrap: [BarcodeGenerationComponent]
})
export class BarcodeGenerationModule { }
