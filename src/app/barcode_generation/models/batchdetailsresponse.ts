export class BatchDetailsResponse {
  Country:any;
  CreatedBy:any;
  CreatedOn:any;
  BatchNumber:any;
  IndexOfEndBarcode:any;
  IndexOfStartBarcode:any;
  KitType:any;
  ModifiedBy:any;
  ModifiedOn:any;
  NumberOfBarcodesGenerated:any;
  Status:any
}
