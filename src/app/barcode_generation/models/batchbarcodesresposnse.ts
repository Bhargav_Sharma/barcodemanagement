
export class BatchBarcodesResponse {
  BatchNumber:any;
  Country:any;
  KitType:any;
  Barcode:any;
  CreatedBy:any;
  CreatedOn:any;
  EmptyColumn1:any;
  EmptyColumn2:any;
}
