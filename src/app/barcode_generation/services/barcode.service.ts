import { Injectable,Component } from '@angular/core';
import { BatchCreation } from '../models/batchcreation';
import { Constants } from '../constants/constants';
import { Observable } from 'rxjs/Observable';
import { Http, Response, Headers } from '@angular/http';
import { environment } from '../../../environments/environment';
import 'rxjs/Rx';

@Injectable()
export class BarcodeService {
    private currentStepName: string;
    private currentStepTitle: string;
    private currentStepInstructionalText: string;
    error: string;

    data: BatchCreation;
    hasStartedProperly: boolean;
    constructor(private _http: Http, private constants:Constants) {
        this.data = new BatchCreation();
        console.log('Batch Creation Service Initialized...');
    }

    public createBatch(country: any, kitType: any, quantity: any): any {
        var response = {};
        var request = {};

        request = {
            "Country": country,
            "KitType": kitType,
            "NumberOfBarcodesGenerated": quantity,
            "CreatedBy": "MyDNA Aministrator" //Todo: After windows authentication card
        }

        response = this.PostAll(this.constants.createBatchApi, request);
        return response;
    }

    public getBatchDetails(batchNumber: any): any {
        var response = {};
        var request = {};

        var actionUrl = this.constants.getBatchDeatilsApi + batchNumber;

        request = {
            "batchNumber": batchNumber
        }

        response = this.GetAll(actionUrl);
        return response;
    }

    public getBatchBarcodeDetails(batchNumberArray: any): any {
        var response = {};
        var request = {};

        request = {
            "BatchIdList": batchNumberArray
        }

        response = this.PostAll(this.constants.getBatchBarcodesApi, request);
        return response;
    }

    
    public GetAll = (actionUrl: string): Observable<any> => {
        return this._http.get(environment.barcodesAPIServer_Dev+actionUrl)
            .map((response: Response) => <any>response.json())
            .do(x => x);

    }

    public PostAll = (actionUrl: string, body: any) :  Observable<any> => {

        var response = {};

        return this._http.post(environment.barcodesAPIServer_Dev+actionUrl, body)
            .map((response: Response) => <any>response.json())
            .do(y => y);
    }

    saveJwt(jwt) {
        if (jwt) {
            localStorage.setItem('id_token', jwt)
        }
    }

}