
export class Constants
{
    
    public createBatchApi = "Api/Batch/CreateBatch";
    public getBatchDeatilsApi = "Api/Batch/GetBatchDetails?batchNumber=";
    public getBatchBarcodesApi = "Api/Batch/GetBatchDetails";
    public header = "BatchNumber,Country,KitType,Barcodes,CreatedBy,CreatedOn,,";
    public fileName = "BatchGenerator";
}