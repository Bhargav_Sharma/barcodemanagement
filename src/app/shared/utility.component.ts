import { Injectable,Component } from '@angular/core';

@Injectable()
export class Utility {

ExportToCSV(csvContent: any, title: any) {
    //create column_names here, sep by commas, append them to "csvContent", end with /n
    //create your data rows sep by commas & quoted, end with /n
    var filename = title.replace(/ /g, '') + '.csv'; //gen a filename using the title but getting rid of spaces
    var blob = new Blob([csvContent], { "type": 'text/csv;charset=utf-8;' });
    if (navigator.msSaveBlob) { // IE 10+
      navigator.msSaveBlob(blob, filename);
    }
    else //create a link and click it
    {
      var link = document.createElement("a");
      if (link.download !== undefined) // feature detection
      {
        // Browsers that support HTML5 download attribute
        var url = URL.createObjectURL(blob);
        link.setAttribute("href", url);
        link.setAttribute("download", filename);
        link.style.visibility = 'hidden';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
      }
    }
  }

  ConvertToCSV(objArray, header) {
    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    var str = '';

    for (var i = 0; i < array.length; i++) {
      var line = '';
      for (var index in array[i]) {
        if (line != '') line += ','

        line += array[i][index];
      }

      str += line + '\r\n';
    }

    return header + '\r\n' +str;
  }

}